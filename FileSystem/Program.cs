﻿using System;
// Adicionando o namespace que lida com acesso a diretórios e arquivos
using System.IO;

namespace FileSystem
{   
    class FileSystem
    {
        static void Main(string[] args)
        {
            string basePath = Directory.GetCurrentDirectory();
            string actionPath = Path.Combine(basePath, "Examples");

            string filePath = Path.Combine(actionPath, "arquivo1.txt");
            string[] lines = { "Primeira Linha", "Segunda linha", "Terceira Linha" };
            using StreamWriter fileStream = new(filePath);

            foreach (var line in lines)
            {
                fileStream.WriteLine(line);
            }

            string[] filesArray = Directory.GetFiles(actionPath);

            foreach (var file in filesArray)
            {
                Console.WriteLine(file);
            }            
        }
    }
}
