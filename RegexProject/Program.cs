﻿using System;
using System.Text.RegularExpressions;

namespace RegexProject{
    class Program{
        static void Main(string[] args){
            String dataAtual = System.DateTime.Now.ToString();
            String text = "Esta é a data e hora atual: " + dataAtual;

            // Criando um objeto Match através do método Match
            Match data = Regex.Match(text, @"\d{2}\/\d{2}\/\d{4}");
            Console.WriteLine(data.ToString());

            // Criando um objeto Regex
            Regex rx = new Regex(@"\d{2}\/\d{2}\/\d{4}");

            // Criando um objeto Match do Objeto Regex
            Match data2 = rx.Match(text);
            Console.WriteLine(data2.ToString());
        }
    }
}
